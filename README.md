# Getting Started

1.1 Elasticsearch 是什么
The Elastic Stack, 包括 Elasticsearch、Kibana、Beats 和 Logstash（也称为 ELK Stack）。
能够安全可靠地获取任何来源、任何格式的数据，然后实时地对数据进行搜索、分析和可视
化。Elaticsearch，简称为 ES，ES 是一个开源的高扩展的分布式全文搜索引擎，是整个 Elastic
Stack 技术栈的核心。它可以近乎实时的存储、检索数据；本身扩展性很好，可以扩展到上
百台服务器，处理 PB 级别的数据。



2.1.1 下载软件
Elasticsearch 的官方地址：https://www.elastic.co/cn/
Elasticsearch 最新的版本是 7.13.2（截止 2021.8.1），我们选择 7.8.01版本
下载地址：https://www.elastic.co/cn/downloads/past-releases#elasticsearch

Elasticsearch 分为 Linux 和 Windows 版本，基于我们主要学习的是 Elasticsearch 的 Java
客户端的使用，所以课程中使用的是安装较为简便的 Windows 版本。

客户端安装
如果直接通过浏览器向 Elasticsearch 服务器发请求，那么需要在发送的请求中包含
HTTP 标准的方法，而 HTTP 的大部分特性且仅支持 GET 和 POST 方法。所以为了能方便
地进行客户端的访问，可以使用 Postman 软件
Postman 是一款强大的网页调试工具，提供功能强大的 Web API 和 HTTP 请求调试。
软件功能强大，界面简洁明晰、操作方便快捷，设计得很人性化。Postman 中文版能够发送
任何类型的 HTTP 请求 (GET, HEAD, POST, PUT..)，不仅能够表单提交，且可以附带任意
类型请求体。
Postman 官网：https://www.getpostman.com
Postman 下载：https://www.getpostman.com/apps

postman测试中的所有url我都导出来了，在es\src\main\resources\ES基本操作.postman_collection.json,要是不想敲，可打开postman，file-import-upload files导入即可