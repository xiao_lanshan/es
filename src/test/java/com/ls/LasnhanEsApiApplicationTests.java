package com.ls;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ls.pojo.User;
import jdk.nashorn.internal.objects.NativeDate;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

@SpringBootTest
class LasnhanEsApiApplicationTests {

	@Test
	void contextLoads() {
	}

	@Qualifier("getRestHighLevelClient")
	@Autowired
	private RestHighLevelClient client;
	/**
	 * 创建索引
	 */
	@Test
	public void test1(){
		//注意索引名要小写
		CreateIndexRequest request = new CreateIndexRequest("springboot_es_test2");
		try {

			CreateIndexResponse createIndexResponse = client.indices().create(request, RequestOptions.DEFAULT);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * 获取索引
	 */
	@Test
	public void test2() throws IOException {
		GetIndexRequest request = new GetIndexRequest("springboot_es_test1");
		try {
			boolean exists = client.indices().exists(request, RequestOptions.DEFAULT);
			System.out.println(exists);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
