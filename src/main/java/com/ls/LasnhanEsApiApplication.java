package com.ls;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LasnhanEsApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(LasnhanEsApiApplication.class, args);
	}

}
