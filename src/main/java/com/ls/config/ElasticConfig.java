package com.ls.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Auther: chenjun
 * @Date 2021年07月29日 21:41
 * @Description: 创建ES的配置类
 */
@Configuration
public class ElasticConfig {
    @Bean
    public RestHighLevelClient getRestHighLevelClient(){
        RestHighLevelClient client = new RestHighLevelClient(
                //如果是集群再配置多个
                RestClient.builder(new HttpHost("127.0.0.1",9200,"http"))
        );

        return client;
    }
}
